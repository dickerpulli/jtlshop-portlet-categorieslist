{get_category_array categoryId=0 assign='categories'}
{if !empty($categories)}
    <hr>
    <div class="kategorieliste">
        <h2>{lang key="shopCategories" section="global"}</h2>
        <div class="row same-width">
        {foreach name=categories from=$categories item=category}
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                {if $category->cBildURL !== 'gfx/keinBild.gif'}
                    <a href="{$category->cURL}">
                        <img src="{$category->cBildURLFull}" class="img-fluid"
                                alt="{$category->cKurzbezeichnung|escape:'html'}">
                    </a>
                    <div class="clearall"></div>
                {/if}
                <h3>
                    <a href="{$category->cURL}">{$category->cKurzbezeichnung}</a>
                </h3>
                <div class="description text-muted small">
                    <a href="{$category->cURL}">{$category->KategorieAttribute.kurzbeschreibung}</a>
                </div>
            </div>
        {/foreach}
        </div>
    </div>
{/if}
